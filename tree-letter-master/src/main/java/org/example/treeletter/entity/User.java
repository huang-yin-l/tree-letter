package org.example.treeletter.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer UserID; // 使用 Integer 类型支持 null 值

    @Column(name = "phone", unique = true, nullable = false)
    private String phone;

    @Column(name = "password", nullable = false) // 修改为小写字母
    private String password;

    @Column(name = "user_name")
    private String userName;

    // Getters and Setters

    public Integer getUserID() {
        return UserID;
    }

    public void setUserID(Integer userID) {
        this.UserID = userID;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getUserId() {
        return UserID;
    }
}