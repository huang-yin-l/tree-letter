package org.example.treeletter.service;

import org.example.treeletter.entity.Comment;
import org.example.treeletter.entity.Message;
import org.example.treeletter.entity.User;

import java.util.List;

public interface MessageService {



    List<Message> getAllMessages();



    List<Comment> getCommentsForMessage(Integer messageID);

    void deleteMessageById(Integer messageID);

    void deleteCommentById(Integer commentID);

    Message getMessageById(Integer messageID);

    // 添加用户注册方法
    void registerUser(User user);

    // 添加用户登录方法
    User loginUser(String phone, String password);


    void saveMessage(String sign, String content, Integer userId);

    List<Message> getMessagesByUserId(Integer userId);

    Message getRandomMessage();

    Message getRandomMessageExcludingUserId(Integer userId);

    void addComment(Integer messageID, String commentText, Integer userId,String userPhone,String creationTime);
}