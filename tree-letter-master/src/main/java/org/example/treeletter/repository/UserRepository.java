package org.example.treeletter.repository;

import org.example.treeletter.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface UserRepository extends JpaRepository<User,Integer > {

    User findByPhone(String phone);
}