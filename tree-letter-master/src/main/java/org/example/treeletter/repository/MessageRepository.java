package org.example.treeletter.repository;

import org.example.treeletter.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Integer> {
    void deleteById(Integer id);


    List<Message> findByUserId(Integer userId);
    List<Message> findAll();

}