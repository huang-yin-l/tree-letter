package org.example.treeletter.repository;

import org.example.treeletter.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

    @Query("SELECT c FROM Comment c WHERE c.message.messageID = :messageId")
    List<Comment> findByMessage_Id(@Param("messageId") Integer messageId);
    List<Comment> findByMessage_MessageID(Integer messageID);

    @Query("SELECT COUNT(c) FROM Comment c WHERE c.message.messageID = :messageId")
    long countByMessage_Id(@Param("messageId") Integer messageId);

}