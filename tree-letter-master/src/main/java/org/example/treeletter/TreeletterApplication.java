package org.example.treeletter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TreeletterApplication {

    public static void main(String[] args) {
        SpringApplication.run(TreeletterApplication.class, args);
    }

}
