/*
 Navicat Premium Data Transfer

 Source Server         : hy
 Source Server Type    : MySQL
 Source Server Version : 80037 (8.0.37)
 Source Host           : localhost:3306
 Source Schema         : letter_tree

 Target Server Type    : MySQL
 Target Server Version : 80037 (8.0.37)
 File Encoding         : 65001

 Date: 24/10/2024 19:27:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `comment_text` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `creation_time` datetime(6) NULL DEFAULT NULL,
  `user_id` int NULL DEFAULT NULL,
  `message_id` int NOT NULL,
  `user_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKbeub3nlpue4r2g65wqbutlyni`(`message_id` ASC) USING BTREE,
  CONSTRAINT `FKbeub3nlpue4r2g65wqbutlyni` FOREIGN KEY (`message_id`) REFERENCES `messages` (`MessageID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES (55, '20021024', '2024-10-23 10:26:24.000000', 3, 58, '18076180067');
INSERT INTO `comment` VALUES (62, '123456', '2024-10-23 10:34:08.000000', 4, 50, '1008611');
INSERT INTO `comment` VALUES (63, '123456789', '2024-10-23 10:36:15.000000', 4, 50, '1008611');
INSERT INTO `comment` VALUES (64, '123456', '2024-10-23 10:37:39.000000', 4, 53, '1008611');
INSERT INTO `comment` VALUES (65, '123456', '2024-10-24 15:32:44.000000', 4, 53, '1008611');

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments`  (
  `CommentID` int NOT NULL AUTO_INCREMENT COMMENT '留言ID',
  `userid` int NULL DEFAULT NULL,
  `MessageID` int NOT NULL COMMENT '用户ID',
  `CommentText` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '留言内容',
  `CommentTime` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '留言时间',
  `comment_text` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `comment_time` datetime(6) NULL DEFAULT NULL,
  `creation_time` datetime(6) NULL DEFAULT NULL,
  `user_id` bigint NULL DEFAULT NULL,
  `message_id` int NOT NULL,
  PRIMARY KEY (`CommentID`) USING BTREE,
  INDEX `MessageID`(`MessageID` ASC) USING BTREE,
  INDEX `FKjxggc60wwwlf4xl065fjrx68y`(`userid` ASC) USING BTREE,
  INDEX `FK6mrnbfthapvp365xmtrpojpu2`(`message_id` ASC) USING BTREE,
  CONSTRAINT `FK6mrnbfthapvp365xmtrpojpu2` FOREIGN KEY (`message_id`) REFERENCES `messages` (`MessageID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK784wu13wk1lisd179j26w9tnw` FOREIGN KEY (`MessageID`) REFERENCES `messages` (`MessageID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKjxggc60wwwlf4xl065fjrx68y` FOREIGN KEY (`userid`) REFERENCES `users` (`UserID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of comments
-- ----------------------------

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages`  (
  `MessageID` int NOT NULL AUTO_INCREMENT COMMENT '信件ID',
  `UserID` int NULL DEFAULT NULL COMMENT '用户ID',
  `Sign` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '信件署名',
  `content` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `CreationTime` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '写信时间',
  `creation_time` datetime NULL DEFAULT NULL,
  `user_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`MessageID`) USING BTREE,
  INDEX `UserID`(`UserID` ASC) USING BTREE,
  CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `users` (`UserID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 61 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES (2, NULL, '2002', 'ss', '2024-10-22 20:39:03', '2024-10-22 12:39:03', 1);
INSERT INTO `messages` VALUES (50, NULL, '18076180067', '123456', '2024-10-22 20:48:54', '2024-10-22 12:48:54', 3);
INSERT INTO `messages` VALUES (51, NULL, '18076180067', 'asd ', '2024-10-22 21:09:09', '2024-10-22 13:09:10', 3);
INSERT INTO `messages` VALUES (53, NULL, '1008611', '哈哈哈哈', '2024-10-23 17:25:58', '2024-10-23 09:25:59', 4);
INSERT INTO `messages` VALUES (54, NULL, '2002', '123465', '2024-10-23 22:49:40', '2024-10-23 14:49:40', 1);
INSERT INTO `messages` VALUES (55, NULL, '2002', '22', '2024-10-24 15:08:47', '2024-10-24 07:08:48', 1);
INSERT INTO `messages` VALUES (56, NULL, '2002', '222', '2024-10-24 15:08:51', '2024-10-24 07:08:52', 1);
INSERT INTO `messages` VALUES (57, NULL, '小烦', '123456', '2024-10-24 15:09:54', '2024-10-24 07:09:54', 3);
INSERT INTO `messages` VALUES (58, NULL, '哈哈哈', '123456789', '2024-10-24 15:10:04', '2024-10-24 07:10:05', 3);
INSERT INTO `messages` VALUES (60, NULL, '18076180067', '中国曾有“君子一言，驷马难追”的俗语，意思是说，有道德的人对每个人都应说到做到，不出尔反尔，说话不算话。诚实守信，说出的话都努力的去做到，是中外两界一直引以为傲的传统美德。　　春秋时期，鲁国有一个叫曾子的人，它深受孔子的教育，不但学问高深，而且为人诚实，不对任何人撒谎，就连孩子也不例外。有一天，曾子的妻子要上街，儿子哭闹着要去，妻子就哄他说：“你在家乖乖等我，我回来就给你杀猪炖肉吃。”孩子信以为真。妻子回来，见曾子正磨刀霍霍，准备杀猪，赶忙阻拦说:\"你真要杀猪给他吃啊?我原是哄他的!”可曾子却认真的说：“对小孩子怎么能欺骗呢?我们的一言一行都对他们有影响，我们说了不算数，孩子以后就不会听我们的话了。”后来，曾子真的为儿子杀猪炖肉吃了。我们的一举一动都影响着身边的孩子，我们不好，他们自然也会不好，我们好，他们自然就也会好了。为了我们的伙伴，同是祖国的花朵，我们要以身作则，在他们面前做好好榜样。　　有一次，一个顾客走进一家汽车维修店，他自称是某运输公司的汽车司机。“在我的账单上多写点零件，我会公司报销后，会有你一份好处。”他对店主说。但店主拒绝了这样的要求。可是，顾客又纠缠说：“我的生意不算小，我一定会常来的，你肯定能赚很多钱!”店主告诉他：“这种事我无论如何都不会干的。”这时，顾客气急败坏的嚷道：“谁都会这么干的，我看你是太傻了。”店主火了，他要那个顾客马上离开，道别处谈这种生意去。突然，顾客露出了满意的笑容，他满怀敬佩的握住店主的手说：“其实，我就是那家运输公司的老板。我一直在寻找一个固定的、信得过的维修店，我今后一定会常来的。”诚实有信誉让人领略到一种山高海深。诚信就是一种闪光的品格。　　列宁八岁时，有一天，在姑妈家玩耍时，不小心打破了姑妈的花瓶，姑妈会到家看见了，便把孩子聚集到一起，问：“是谁打碎了花瓶呀?”小列宁出于害怕，就与其他孩子一起数：“不是我!”后来，列宁的母亲知道了是他干的，却不对他说，只是每天给他讲关于诚信美德的故事，有一天，母亲讲到一半时，忽然，小列宁哭了起来，他惭愧的告诉妈妈：“妈妈，对不起，我欺骗了姑妈，那花瓶就是我打碎的。”母亲耐心的安慰着列宁，她帮助他写了一封道歉信给姑妈，特地向姑妈道歉。后来，姑妈原谅了列宁，还夸列宁是个勇敢的孩子呢。可见，讲诚信，不仅沟通', '2024-10-24 16:13:21', '2024-10-24 08:13:21', 3);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `UserID` int NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`UserID`) USING BTREE,
  UNIQUE INDEX `UserName`(`phone` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '2002', '2002', NULL);
INSERT INTO `users` VALUES (2, '1234567890', 'password123', NULL);
INSERT INTO `users` VALUES (3, '18076180067', '123456', NULL);
INSERT INTO `users` VALUES (4, '1008611', '123456', NULL);

SET FOREIGN_KEY_CHECKS = 1;
